package com.tnwt.raithong.csa.entity;

import static org.junit.Assert.*;
import org.junit.*;
import java.math.BigDecimal;

public class When_working_with_Subscription {
	private Subscription sub;

	@Before
	public void setup() {
		sub = new Subscription();
	}

	@Test
	public void and_create_subscription_then_price_should_be_equal_to_0() {
		BigDecimal expectValue = new BigDecimal("0");
		assertEquals(expectValue, sub.getPrice());
	}

	@Test
	public void then_price_should_not_be_set_to_less_than_0() {
		try {
			BigDecimal testValue = new BigDecimal("-0.01");
			sub.setPrice(testValue);
		} catch (Exception e) {
			assertEquals(Subscription.LESS_THAN_ZERO_PRICE_EXCEPTION_MSG,
					e.getMessage());
		}
	}

	@Test
	public void then_price_should_not_be_set_to_null() {
		try {
			sub.setPrice(null);
		} catch (Exception e) {
			assertEquals(Subscription.NULL_PRICE_EXCEPTION_MSG, e.getMessage());
		}
	}

	@Test
	public void then_duration_should_be_able_to_set() {
		sub.setDuration(SubsriptionDuration.LONG);
		assertEquals(SubsriptionDuration.LONG, sub.getDuration());
	}

	@Test
	public void then_munchingBox_should_be_able_to_set() {
		MunchingBox expectedMunch = new MunchingBox();
		sub.setMunchingBox(expectedMunch);
		MunchingBox actualMunch = sub.getMunchingBox();
		assertEquals(expectedMunch,actualMunch);
		assertEquals(10,actualMunch.getItemCount());
	}
}
