package com.tnwt.raithong.csa.entity;

import static org.junit.Assert.*;
import org.junit.*;

public class When_working_with_Item {

	private Item item;
	private String name;

	@Before
	public void setUp() {
		name = "testName";
		item = new Item(name);
	}

	@Test
	public void and_create_item_then_name_should_be_specified() {
		String expectName = item.getName();
		assertEquals(expectName, name);
	}

	@Test
	public void and_create_item_with_null_name_then_exception_should_be_throw() {
		try {
			item = new Item(null);
		} catch (Exception ex) {
			assertEquals(Item.NULL_NAME_EXCEPTION_MSG, ex.getMessage());
		}
	}

	@Test
	public void and_create_item_with_empty_string_name_then_exception_should_be_throw() {
		try {
			item = new Item("");
		} catch (Exception ex) {
			assertEquals(Item.EMPTY_NAME_EXCEPTION_MSG, ex.getMessage());
		}
	}

	@Test
	public void then_name_should_be_able_to_set() {
		String expectName = "testName2";
		item.setName(expectName);
		assertEquals(expectName, item.getName());
	}

	@Test
	public void and_set_name_with_null_then_exception_should_be_throw() {
		try {
			item.setName(null);
		} catch (Exception ex) {
			assertEquals(Item.NULL_NAME_EXCEPTION_MSG,ex.getMessage());
		}
	}
	
	@Test
	public void and_set_name_with_empty_string_then_exception_should_be_throw() {
		try {
			item.setName("");
		} catch (Exception ex) {
			assertEquals(Item.EMPTY_NAME_EXCEPTION_MSG,ex.getMessage());
		}
	}

}
