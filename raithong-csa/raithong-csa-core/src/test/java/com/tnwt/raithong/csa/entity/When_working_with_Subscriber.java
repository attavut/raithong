package com.tnwt.raithong.csa.entity;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.Test;

public class When_working_with_Subscriber {

	private Subscriber a;
	private String initEmail;

	@Before
	public void setUp() {
		initEmail = "a@b.com";
		a = new Subscriber(initEmail);
	}

	@Test
	public void and_create_subscriber_then_email_should_be_specified() {
		String ExpectEmail = a.getEmail();
		assertEquals(ExpectEmail, initEmail);
	}

	@Test
	public void and_create_subscriber_with_null_email_then_exception_should_be_thrown() {
		try {
			a = new Subscriber(null);
			fail();
		} catch (Exception ex) {
			assertEquals(Subscriber.NULL_EMAIL_EXCEPTION_MSG, ex.getMessage());
		}

	}

	@Test
	public void and_create_subscriber_with_empty_email_then_exception_should_be_thrown() {
		try {
			a = new Subscriber("");
			fail();
		} catch (Exception ex) {
			assertEquals(Subscriber.EMPTY_EMAIL_EXCEPTION_MSG, ex.getMessage());
		}
	}

	@Test
	public void then_email_should_be_able_to_set() {
		initEmail = "b@b.com";
		a.setEmail(initEmail);
		assertEquals(initEmail, a.getEmail());
	}

	@Test
	public void then_subscription_should_be_able_to_set() {
		Subscription subTest = new Subscription();
		a.setSubscription(subTest);
		assertEquals(subTest, a.getSubscription());
	}

	@Test
	public void and_set_email_with_null_then_exception_should_be_throw() {
		try {
			a.setEmail(null);
			fail();
		} catch (Exception ex) {
			assertEquals(Subscriber.NULL_EMAIL_EXCEPTION_MSG, ex.getMessage());
		}
	}

	@Test
	public void and_set_email_with_empty_string_then_exception_should_be_throw() {
		try {
			a.setEmail("");
			fail();
		} catch (Exception ex) {
			assertEquals(Subscriber.EMPTY_EMAIL_EXCEPTION_MSG, ex.getMessage());
		}
	}

}
