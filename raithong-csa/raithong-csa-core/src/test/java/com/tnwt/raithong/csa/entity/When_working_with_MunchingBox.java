package com.tnwt.raithong.csa.entity;

import static org.junit.Assert.*;
import org.junit.*;

public class When_working_with_MunchingBox {

	private MunchingBox m;

	@Before
	public void setUp() {
		m = new MunchingBox();
	}

	@Test
	public void and_create_munchingbox_then_list_should_be_have_10_items() {
		assertEquals(m.getItemCount(), MunchingBox.MAX_ITEM_COUNT);
	}

	@Test
	public void and_get_item_from_list_then_should_be_able_to_get_by_index() {
		int index = 0;
		Item item = m.getItem(index);
		assertEquals(item.getName(), "name1");

		index = 4;
		item = m.getItem(index);
		assertEquals(item.getName(), "name5");

		index = 9;
		item = m.getItem(index);
		assertEquals(item.getName(), "name10");

	}

	@Test
	public void and_get_item_from_list_by_index_less_than_0_then_exception_should_be_throw() {
		try {
			m.getItem(-1);
		} catch (Exception ex) {
			assertEquals(MunchingBox.OUTOFRANGE_INDEX_EXCEPTION_MSG,
					ex.getMessage());
		}

	}

	@Test
	public void and_get_item_from_list_by_index_equal_0_then_exception_should_be_throw() {
		try {
			m.getItem(0);
		} catch (Exception ex) {
			assertEquals(MunchingBox.OUTOFRANGE_INDEX_EXCEPTION_MSG,
					ex.getMessage());
		}

	}

	@Test
	public void and_get_item_from_list_by_index_more_than_upper_bound_then_exception_should_be_throw() {
		try {
			m.getItem(11);
		} catch (Exception ex) {
			assertEquals(MunchingBox.OUTOFRANGE_INDEX_EXCEPTION_MSG,
					ex.getMessage());
		}

	}

	@Test
	public void then_size_should_be_able_to_set() {
		m.setSize(MunchingBoxSize.S);
		assertEquals(MunchingBoxSize.S, m.size());
	}

}
