package com.tnwt.raithong.csa.entity;

public class Item {
	public static final String NULL_NAME_EXCEPTION_MSG = "Name can not be null.";
	public static final String EMPTY_NAME_EXCEPTION_MSG = "Name can not be empty string.";

	private String name;

	public String getName() {
		return this.name;
	}

	public Item(String name) {
		if (name == null)
			throw new RuntimeException(NULL_NAME_EXCEPTION_MSG);
		else if (name == "")
			throw new RuntimeException(EMPTY_NAME_EXCEPTION_MSG);
		this.name = name;
	}

	public void setName(String name) {
		if (name == null)
			throw new RuntimeException(NULL_NAME_EXCEPTION_MSG);
		else if (name == "")
			throw new RuntimeException(EMPTY_NAME_EXCEPTION_MSG);
		this.name = name;
	}

}
