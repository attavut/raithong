package com.tnwt.raithong.csa.entity;

import java.math.BigDecimal;

public class Subscription {
	public static final String LESS_THAN_ZERO_PRICE_EXCEPTION_MSG = "Price should be more than 0.";
	public static final String NULL_PRICE_EXCEPTION_MSG = "Price should not be null.";
	
	private SubsriptionDuration duration;
	private BigDecimal price;
	private MunchingBox munchingBox;

	public SubsriptionDuration getDuration() {
		return this.duration;
	}

	public void setDuration(SubsriptionDuration value) {
		this.duration = value;
	}

	public BigDecimal getPrice() {
		return this.price;
	}

	public void setPrice(BigDecimal price) {
		if (price == null)
			throw new RuntimeException(NULL_PRICE_EXCEPTION_MSG);
		else if (price.compareTo(BigDecimal.ZERO) < 0)
			throw new RuntimeException(LESS_THAN_ZERO_PRICE_EXCEPTION_MSG);
		this.price = price;
	}

	public Subscription() {
		this.price = BigDecimal.ZERO;
	}

	public void setMunchingBox(MunchingBox munch) {
		this.munchingBox = munch;
	}

	public MunchingBox getMunchingBox() {
		return this.munchingBox;
	}

}
