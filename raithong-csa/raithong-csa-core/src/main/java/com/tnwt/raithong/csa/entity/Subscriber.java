package com.tnwt.raithong.csa.entity;

public class Subscriber {
	public static final String EMPTY_EMAIL_EXCEPTION_MSG = "Email can not be empty string.";
	public static final String NULL_EMAIL_EXCEPTION_MSG = "Email can not be null.";
	
	private Subscription subscription;
	private String email;
	
	public void setSubscription(Subscription sub) {
		this.subscription = sub;
	}
	
	public Object getSubscription() {
		return this.subscription;
	}
	
	public void setEmail(String string) {
		if(string == null)
			throw new RuntimeException(NULL_EMAIL_EXCEPTION_MSG);
		else if(string =="")
			throw new RuntimeException(EMPTY_EMAIL_EXCEPTION_MSG);
		this.email = string;
	}
	
	public String getEmail() {
		return this.email;
	}
	
	public Subscriber(String email){
		if(email == null)
			throw new RuntimeException(NULL_EMAIL_EXCEPTION_MSG);
		else if(email == "")
			throw new RuntimeException(EMPTY_EMAIL_EXCEPTION_MSG);
		this.email = email;
	}
	
	
	
}
